
module.exports = function (grunt){
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin'
    });


grunt.initConfig({
    sass:{
        dist:{
            files: [{
                expand:true,
                cwd:'css',
                src:['*.scss'],
                dest:'css',
                ext:'.css'
            }]
        }
    },

    watch: {
        files:['css/*.scss'],
        tasks:['css'] /*este nombre es el de la tarea de grunt, que en este caso es css*/
    },

    browserSync: {
        dev:{
            bsFiles: { //browser files
                src:[
                    'css/*.css',
                    '*html',
                    'js/*.js'
                ]
            },

        options: {
            watchTask: true,  //tarea de forma constante, actualiza cambios en el explorador
            server:   {
                    baseDir: './' //Directorio base para nuestro servidor
                }         
            }
        }
    },

    imagemin: {
        dynamic:{
            files:[{
                expand: true,
                cwd:'./',
                src:'imagenes/*.{png,gif,jpg,jpeg,webp}',
                src: ['imagenes/*.{png,gif,jpg,jpeg,webp}','imagenes/Carrousel/*.{png,gif,jpg,jpeg,webp}'],
                dest:'dist/'
            }]

        }
    },

    copy: {
        html: {
            files:[{
            expand: true,
            dot:true,
            cwd: './',
            src: ['*html'],
            dest:'dist'
            }]
        },
        fonts:{
            files: [{
                expand: true,
                dot:true,
                cwd: 'node_modules/open-iconic/font',
                src: ['fonts/*.*'],
                dest:'dist'
                }]
        }

    },

    clean: {
        build: {
            src:['dist/']
        }
    },

    ccsm: {
        dist: {}
    },

    uglify: {
        dist:{}
    },

    filerev: {
        options: {
            encoding: 'utf8',
            algorithm:'md5',
            length:20
        },

        release: {
            files: [{
                src: ['dist/js/*.js','dist/css/*.css',]
            }]
        }
    },

    concat: {
        options: {
            separator:';'
        },
        dist:{}

    },


    useminPrepare: {
        foo: {
            dest: 'dist',
            src: ['index.html','Nosotros.html', 'Precios.html', 'Terminos_y_Condiciones.html', 'Contacto.html']
            },
        options: {
                flow: {
                steps: {
                    css: ['cssmin'],
                    js: ['uglify']
                    },
                post: {
                    css: [{
                    name: 'cssmin',
                    createConfig: function(context, block) {
                    var generated = context.options.generated;
                    generated.options = {
                    keepSpecialComments: 0,
                    rebase: false
                            }
                        }
                    }]
                }
            }
        }
    },

    usemin: {
        html: ['dist/index.html', 'dist/Nosotros.html', 'dist/Precios.html', 'dist/Terminos_y_Condiciones.html', 'dist/Contacto.html'],
        options: {
            assetsDir: ['dist', 'dist/css', 'dist/js']
        }
    }

});

//grunt.loadNpmTasks('grunt-contrib-watch'); /*se carga la tarea - watch*/
//grunt.loadNpmTasks('grunt-contrib-sass'); /*se carga la tarea - css*/
//grunt.loadNpmTasks('grunt-browser-sync');
//grunt.loadNpmTasks('grunt-contrib-imagemin');
//grunt.loadNpmTasks('grunt-contrib-clean');
//grunt.loadNpmTasks('grunt-contrib-copy');
//grunt.loadNpmTasks('grunt-contrib-uglify');
//grunt.loadNpmTasks('grunt-contrib-cssmin');
//grunt.loadNpmTasks('grunt-usemin');
//grunt.loadNpmTasks('grunt-filerev');   cuando agrero el jit-grunt, puedo borrar los loadNpmTask


grunt.registerTask('css', ['sass']);  /* se registra la tarea y se le asigna un nombre 'css'*/
grunt.registerTask('default', ['browserSync', 'watch']);
grunt.registerTask('img:compress', ['imagemin']);
grunt.registerTask('build', ['clean','copy','imagemin','useminPrepare','concat','cssmin','uglify','filerev','usemin']);

};

