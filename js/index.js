$(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('#btnReserva').popover({
        placement : 'top',
        trigger : 'hover'
    });


    $('[data-toggle="popover"]').popover();

    $('#carousel_index').carousel({
      interval: 1000
  });

    $('#modalContacto').on('show.bs.modal', function (e) {
      console.log('El modal se esta mostrando');



        $('.btnContacto').removeClass('btn-primary');
        $('.btnContacto').addClass('btn-warning');
        $('.btnContacto').prop('disabled', true);

    });

    $('#modalContacto').on('hide.bs.modal', function (e) {
      console.log('El modal se esta ocultando');

      $('.btnContacto').prop('disabled', false);
      $('.btnContacto').removeClass('btn-warning');
      $('.btnContacto').addClass('btn-primary');

    });

    $('#modalContacto').on('shown.bs.modal', function (e) {
      console.log('El modal se mostro');
    });

    $('#modalContacto').on('hidden.bs.modal', function (e) {
      console.log('El modal se oculto');
    });

})